# Repositório da Segunda Semana do Challenge DevOps - Alura
<p>&nbsp;</p>

- [Repositório da Segunda Semana do Challenge DevOps - Alura](#repositório-da-segunda-semana-do-challenge-devops---alura)
  - [Introdução](#introdução)
  - [Requisitos para subir a aplicação na AWS](#requisitos-para-subir-a-aplicação-na-aws)
  - [Passos para subir a aplicação na Cloud](#passos-para-subir-a-aplicação-na-cloud)
<p>&nbsp;</p>  

## Introdução

O desafio da segunda semana consiste em subir o conteiner da aplicação backend Aluraflix, criado na primeira semana, em um provedor Cloud de sua preferência.

Neste desafio, escolhi o provedor cloud AWS (Amazon Web Services), onde irei subir a aplicação em um ambiente Docker, utilizando os serviços abaixo: 

- **Amazon ECR** (Elastic Container Registry) = utilizado para criar um repositório e assim subir a imagem docker da aplicação na AWS;
- **Amazon S3** (Simple Storage Service) = utilizado para armazenar o arquivo de Docker run, obrigatório para serviço escolhido para executar a aplicação;
- **Amazon Elastic Beanstalk** = utilizado para fazer a implementação da aplicação, onde utilizarei o Docker para subir a aplicação.
O Elastic Beanstalk será utilizado usando auto-scaling com no máximo cinco instâncias.

Para o deploy do ambiente, será utilizado o Terraform, que é uma linguagem de infraestrutura como código (IaC), que agiliza a criação do ambiente, facilitando a recriação do mesmo ambiente em diversos provedores de Cloud, reutilizando o mesmo código.
<p>&nbsp;</p>

## Requisitos para subir a aplicação na AWS

Para subir a aplicação na AWS, você precisa de ter uma conta criada e um cartão de crédito cadastrado.
Caso seja somente para testes, certifique-se que sua conta esteja apta a usar o free tier, que é o periodo de 12 meses a contar da criação da conta, que você poderá utilizar os serviços elegiveis de forma gratuita na Amazon AWS.

Não me responsabilizo pelo uso indevido dos serviços e fora do escopo do nivel gratuito da AWS. A conta é sua! É de sua total responsabilidade a utilização da mesma!

Tenha instalado em sua máquina o aplicativo **terraform**, que pode ser baixado em: https://www.terraform.io/downloads.

Certifique-se de ter instalado também o **AWS CLI**, que pode ser baixado em: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#cliv2-linux-install.

E por último, instale o git em sua máquina para fazer o clone deste repositório.

<p>&nbsp;</p>

## Passos para subir a aplicação na Cloud

1. Faça o clone deste repositório para a sua máquina local com o comando abaixo:

```zsh
git clone https://gitlab.com/zeduxbr/challenge-devops-sem-02.git
```

2. Entre no diretório do repositório criado:

```zsh
cd challenge-devops-sem-02 
```

3. Entre no diretório Infra:

```zsh
cd Infra
```

4. Inicialize o terraform no diretório com o comando abaixo:

```zsh
terraform init
```

5. Rode o terraform com a opção "**plan**" e verifique os itens que serão criados e veja se não há nenhum erro na saida do comando:

```zsh
terraform plan
```

6. Estando tudo certo, rode o comando terrafom com a opção "**apply**" para criar os serviços e regras definidas no nosso código:

```zsh
terraform apply
```

Confirme se deseja criar respondendo a "**yes**" na execução do comando.

7. criado as bases para que a nossa aplicação seja executada, entre no diretório "**env**" e em seguida no diretório "**Prod**":

```zsh
cd ../env/Prod
```

8. Altere os campos "**bucket**" e "**region**" no arquivo **backend.tf** para a sua região e seu bucket no AWS.
    

9.  Dentro do diretório, execute os comandos terraform init, plan e apply:

```zsh
terraform init
```

```zsh
terraform plan
```

```zsh
terraform apply
```

Confirme com "**yes**" para dar sequência a criação da aplicação no **Elastic Beanstalk**.


10. Criado a aplicação, devemos subir a imagem docker da aplicação, mas antes, devemos nos autenticar no ECR:
    
```zsh
aws ecr get-login-password --region "sua-regiao-na-aws" | docker login --username AWS --password-stdin "seu-id-na-aws".dkr.ecr."sua-regiao-na-aws".amazonaws.com
```

Confirme o login se foi feito com sucesso.

11.  Renomeie a imagem local para o padão utilizado pelo ECR:

```zsh
docker tag "id-da-imagem-local" "seu-id-na-aws".dkr.ecr."sua-regiao-na-aws".amazonaws.com/"nome-da-sua-aplicacao":"tag"
```

12.  Feito o passo acima, envie a imagem para seu repositório ECR:

```zsh
docker push "id-da-imagem-local" "seu-id-na-aws".dkr.ecr."sua-regiao-na-aws".amazonaws.com/"nome-da-sua-aplicacao":"tag"
```

Aguarde o termino do envio da imagem para o ECR.


13. Atualize o ambiente de produção após o envio da imagem:

```zsh
aws elasticbeanstalk update-environment --environment-name ambiente-de-producao-aluraflix --version-label ambiente-de-producao-aluraflix
```

Verifique se o ambiente foi atualizado no dashboard da aws e clique sobre o link gerado pelo Beanstalk.

Se tudo estiver certo, você verá a tela da aplicação no seu navegador.